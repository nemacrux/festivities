# Festivities API

This project contains a RESTful API that allows clients to manage festivities (special times different places have to celebrate their local habits, manners,historical dates and other events).

The API lets clients perform next operations:
- Query for all existent festivities
- Query festivities by:
  - name
  - id
  - start date
  - data range
  - name of the place where they are celebrated
- Create new festivities
- Update existent festivities:
  - Update the festivity name
  - Update start date
  - Update end date

## **API Documentation**

###### The API path is *http://{HOST}:{PORT}/festivities/webapi*.
You can execute the operations shown in the next table (and in tables 3 and 4) adding the URI value to the path.
##### Table 1. Festivities operations

| Operation        | URI                         	  | Method  | Result                        				     |
| ---------------  |:---------------------------------|:-------:|----------------------------------------------------|
| Get festivity    |/festivities                 	  |GET      |All festivities                		  		     |
|                  |/festivities/{festivityName} 	  |GET      |All festivities with name equals to {festivityName} |
|                  |/festivities/id/{festivityId}	  |GET      |Festivity with id equals to {festivityId}   		 |
| Edit festivity   |/festivities/{festivityId}   	  |PUT      |Update festivity with the given {festivityId}     	 |
| Create festivity |/festivities                 	  |POST     |Create a new festivity         				     |


- /{festivityId} **refers** to the festivity Id. This Id is assigned by the API.


##### Table 2. Query parameters

| Parameter       | Use                                 | Constraints      			    | Result  		    								|
| --------------  |:----------------------------------- |------------------------------ |---------------------------------------------------|
| place           |/festivities?place=value             |Exact name                     |All festivities celebrated in the given place 		|
| start           |/festivities?start=value             |format *ddMMyyyy* 				|All festivities starting in the given date 		|
| end             |/festivities?start=value&end=value   |format *ddMMyyyy*, start < end |All festivities celebrated between the given range |


##### Table 3. Status codes for GET/PUT/POST according to result

|HTTP Method | Result		     | HTTP Status code |
|:----------:|-------------------|:----------------:|
|GET         |Success            | 200              |
|GET         |Not found          | 404              |
|GET		 |Failure            | 500              |
|PUT		 |Success            | 200              |
|PUT		 |Wrong data/format  | 400              |
|PUT		 |Not found          | 404              |
|PUT		 |Failure            | 500              |
|POST		 |Success            | 201              |
|POST		 |Wrong format/data  | 400              |
|POST		 |Failure            | 500              |



### **Edit festivity operation**

This operation **requires** the *{festivityId}* in the URL to specify the festivity to be changed. It also requieres a body message in JSON/XML with the new data. You only can change the **name**, **start date** or **end date**. It is possible to send different combinations of those values in the same request.

##### Example: body content for an edit request

```javascript
{
    "endDate": "01012016",
    "name": "new name",
    "starDate": "01012015"
}
```
Please note that the date format must be *ddMMyyyy* and the start date **should never be greater or equals than** the end date. Spaces in the name will be replaced by a hyphen.

##### Example: body content for an edit request that only changes the name

```javascript
{
    "name": "new name"
}
```

### **Create festivity operation**

This operation requires a body message in JSON/XML with the festivity **name**, **place id**, **start date** and **end date**. The place value is a JSON object. Note that all this data are mandatory.

##### Example: body content for a creation request
```javascript
{
    "name": "New event name",
    "place": {
      "id": 1,
    },
    "starDate": "2016-02-22T19:16:01.001Z",
    "endDate": "2016-02-23T19:16:01.001Z"
  }
```

For creation dates format must be *yyyy-MM-dd'T'HH:mm:ss.SSSZ* and the start date **should never be greater or equals** than the end date. Spaces in the name will be replaced by a hyphen.

##### Example: response for a creation successful request

```javascript
{
  "endDate": "2016-07-30T19:16:01.001Z",
  "id": 999,
  "name": "New-event-name",
  "place": {
    "id": 1,
    "name": "Clark's-castle"
  },
  "starDate": "2016-02-22T19:16:01.001Z"
}
```
Please note that the API assigns automatically and id for each new festivity and returns it to the client in the response. In this case the id is 999.

##### Example: response for a bad creation request
Missing values in the request will generate an error message. The next example shows the response for a creation operation without the value **name**.

 ```javascript
 {
  "documentation": "doc.festivities.api.errors",
  "errorCode": 400,
  "errorMessage": "Empty or missing name"
}
```

This message is generated for any request that requieres data and it is not sent. The error code is a customized error code, but according to table 1, the response also has a HTTP 400 status code.


##### Table 4. Places operations

| Operation        | URI                         | Method  |Result   						|
| ---------------  |:---------------------------:| :------:|:-------------------------------|
| Get place        |/places                      |GET      |Return all places in the system |


##### Example: response for a /places request

 ```javascript
[
  {
    "id": 1,
    "name": "Clark's-castle"
  },
  {
    "id": 2,
    "name": "West's-joint"
  },
  ...
]
```
Using this result clients can get the place id for a desired place.

### **Content negotiation**

The API is able to process request and sent responses in both JSON and XML formats. Clients only have to indicate what kind of content are sending with the header **Content type** set to *application/json* or *text/xml* according to the request. To receive responses in a specific format they have to set the header **Accept** to one of those values.
