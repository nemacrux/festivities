# API Deployment instructions

1. Download the project.
2. Open a console window in the project root folder.
2. Execute next command:

  ```
  mvn package
  ```

3. Look for *Building war* message:

  ```
  ...
  [INFO] Building war: /path/to/festivities/generated/file/festivities.war
  ...
  ```

  This line contains the path to **festivities.war**.

4. Deploy the file **festivities.war** on Tomcat using your favourite method.
5. Read the [API-Documentation.md](API-Documentation.md) file included in the repository to use the API.

#### Running tests

Before run unit-test for api/model please deploy locally **festivities.war** since testing is carried out using a local external container. After this please open a consolse window, go to project root folder and execute next command:

```
mvn test -Djersey.test.host=localhost -Djersey.config.test.container.port=8080
```

#### Tech stack

- Tomcat 7.0.69
- maven 3.16
- Java 7
- Java EE 6
- Jersey 2.16
