package com.prodigious.test.festivities.loader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prodigious.test.festivities.model.Festivity;
import com.prodigious.test.festivities.model.Place;
import com.prodigious.test.festivities.service.IFestivityService;
import com.prodigious.test.festivities.service.IPlaceService;

/**
 * Reads an XML file with festivities and transform data to be loaded into a database. 
 * @author nmarin
 *
 */
public class XmlFestivitiesLoader {

	private IFestivityService festivityService;
	private IPlaceService placeService;

	// collections to hold objects during the parsing process
	private List<Festivity> festivitiesList;
	private List<Place> placesList;

	//festivity fields
	private static final String FESTIVITY = "festivity";
	private static final String NAME = "name";
	private static final String PLACE = "place";
	private static final String START = "start";
	private static final String END = "end";
	
	private static final Logger logger = LoggerFactory.getLogger(XmlFestivitiesLoader.class.getSimpleName());

	/**
	 * Constructor.
	 * @param festivityService IFestivityService implementation Service to load festivities data into a database.
	 * @param placeService IPlaceService implementation Service to load festivities data into a database.
	 */
	public XmlFestivitiesLoader(IFestivityService festivityService, IPlaceService placeService) {
		this.festivityService = festivityService;
		this.placeService = placeService;
		this.festivitiesList = new ArrayList<>();
		this.placesList = new ArrayList<>();
	}

	/**
	 * Parse XML file.
	 * @param filename The XML filename.
	 */
	public void parse(String filename) {

		try {

			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			InputStream in = XmlFestivitiesLoader.class.getClassLoader().getResourceAsStream(filename);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			Festivity festivity = null;

			// start the document reading
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					// if a festivity is found, then create a new instance of festivity
					if (startElement.getName().getLocalPart() == (FESTIVITY)) {
						festivity = new Festivity();
					}
					
					// find out what tag is currently read and extract information					
					if (event.isStartElement()) {
						if (event.asStartElement().getName().getLocalPart().equals(NAME)) {
							event = eventReader.nextEvent();
							String festName = event.asCharacters().getData().replace(" ", "-").trim();
							festivity.setName(festName);
							continue;
						}
					}

					// if a place is found, extract data and create a new instances of Place
					if (event.asStartElement().getName().getLocalPart().equals(PLACE)) {
						event = eventReader.nextEvent();
						String placeName = event.asCharacters().getData().replace(" ", "-").trim();
						Place place = new Place(-1, placeName);
						placesList.add(place);
						festivity.setPlace(place);
						continue;
					}

					if (event.asStartElement().getName().getLocalPart().equals(START)) {
						event = eventReader.nextEvent();
						event.asCharacters().getData();
						festivity.setStarDate(event.asCharacters().getData());
						continue;
					}

					if (event.asStartElement().getName().getLocalPart().equals(END)) {
						event = eventReader.nextEvent();
						festivity.setEndDate(event.asCharacters().getData());
						continue;
					}
				}

				// if final tag was reached, then store the Festivity instance
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					if (endElement.getName().getLocalPart() == (FESTIVITY)) {
						festivitiesList.add(festivity);
					}
				}
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}
		
		// Add places and festivities to database using services
		for (Place place : placesList) {
			placeService.addPlace(place);
		}
		
		for (Festivity festivity : festivitiesList) {
			if(!isTheSameDate(festivity.getStarDate(), festivity.getEndDate())){
				festivityService.addFestivity(festivity);
			} else {
				logger.error("Event "+festivity.getName()+" has the same start/end date");
			}
			
		}		
	}

	private boolean isTheSameDate(String starDate, String endDate) {
		return starDate.equals(endDate);
	}

}