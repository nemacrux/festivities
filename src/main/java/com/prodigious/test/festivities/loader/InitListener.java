package com.prodigious.test.festivities.loader;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.prodigious.test.festivities.service.DummyFestivityService;
import com.prodigious.test.festivities.service.DummyPlaceService;
import com.prodigious.test.festivities.service.IFestivityService;
import com.prodigious.test.festivities.service.IPlaceService;

/**
 * Servlet used to execute an ETL process to festivities.xml file on application startup.
 * @author nmarin
 *
 */
@WebListener
public class InitListener implements ServletContextListener {

	private static final Logger logger = LoggerFactory.getLogger(InitListener.class.getSimpleName());
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		logger.info("Initializing...");	
		//create services to inject on loader. Those are used to load data in DummyDb
		IFestivityService festivityService = new DummyFestivityService();
		IPlaceService placeService = new DummyPlaceService();		
		XmlFestivitiesLoader loader = new XmlFestivitiesLoader(festivityService, placeService);
		String filename = "festivities.xml";
		logger.info("Parsing XML file "+filename);		
		loader.parse(filename);		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		logger.info("Shutting down...");		
	}

}
