package com.prodigious.test.festivities.service;

import java.util.List;

import com.prodigious.test.festivities.model.Festivity;

/**
 * This interface defines methods to query stored festivities in the database and
 * create new ones.
 * 
 * @author nmarin
 *
 */
public interface IFestivityService {

	List<Festivity> getAllFestivities();

	List<Festivity> getFestivityByName(String name);

	List<Festivity> getFestivitiesByStartDate(String startDate);

	List<Festivity> getFestivitiesByDateRange(String startDate, String endDate);

	List<Festivity> getFestivitiesByPlaceName(String placeName);

	Festivity addFestivity(Festivity festivity);

	Festivity updateFestivityName(Festivity festivity);

	Festivity updateFestivityStartDate(Festivity festivity);

	Festivity updateFestivityEndDate(Festivity festivity);
	
	Festivity updateFestivityDateRange(Festivity festivity);

	Festivity updateFestivity(Festivity festivity);

	Festivity getFestivityById(Long festivityId);	
}
