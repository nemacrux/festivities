package com.prodigious.test.festivities.service;

import java.util.List;

import com.prodigious.test.festivities.model.Place;

/**
 * This interfaces defines methods to query and store places in the database.
 * 
 * @author nmarin
 *
 */
public interface IPlaceService {
	
	List<Place> getAllPlaces();	
	Place addPlace(Place place);
}
