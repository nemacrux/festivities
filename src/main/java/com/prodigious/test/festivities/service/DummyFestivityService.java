package com.prodigious.test.festivities.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormatter;

import com.prodigious.test.festivities.database.DummyDb;
import com.prodigious.test.festivities.model.ErrorMessage;
import com.prodigious.test.festivities.model.Festivity;
import com.prodigious.test.festivities.model.Place;
import com.prodigious.test.festivities.utils.DataValidation;
import com.prodigious.test.festivities.utils.DateUtils;
import com.prodigious.test.festivities.utils.Throw;

/**
 * Offer different services to query the database. Resolves all the client queries.
 * 
 * @author nmarin
 *
 */
public class DummyFestivityService implements IFestivityService {

	// formatters to deal with dates
	private static final DateTimeFormatter dateFormatter = DateUtils.dateFormatter;
	private static final DateTimeFormatter simpleDateformatter = DateUtils.simpleDateformatter;
	// access to database structures.
	private Map<Long, Festivity> festivities = DummyDb.getFestivities();
	private Map<Long, Place> places = DummyDb.getPlaces();

	/**
	 * Return a <code>List</code> of <code>Festivity</code> with all festivities in the database.
	 */
	public List<Festivity> getAllFestivities() {
		ArrayList<Festivity> festivitiesList = new ArrayList<Festivity>(festivities.values());	
		return festivitiesList;		
	}

	/**
	 * Return a <code>List</code> of <code>Festivity</code> with festivities in the database that match the 
	 * specified name.
	 * 
	 * @param name String the festivity name.
	 * 
	 */
	public List<Festivity> getFestivityByName(String name) {
		ArrayList<Festivity> filteredList = new ArrayList<Festivity>();
		for (Festivity fest : festivities.values()) {
			if (fest.getName().equals(name)) {
				filteredList.add(fest);
			}
		}
		return filteredList;
	}
	
	/**
	 * Returns a <code>Festivity</code> matching the given id.
	 * 
	 * @param festivityId long the festivity id.
	 * 
	 */
	@Override
	public Festivity getFestivityById(Long festivityId) {
		if(festivityIdExists(festivityId)){			
			return festivities.get(festivityId);			
		}
		
		return null;
		
	}	
	

	/**
	 * Return a <code>List</code> of <code>Festivity</code> with festivities in the database that match the 
	 * specified start date.
	 */
	public List<Festivity> getFestivitiesByStartDate(String startDateStr) {
		ArrayList<Festivity> filteredList = new ArrayList<Festivity>();
		DateTime startDate = DateUtils.parseSimpleDate(startDateStr);
		for (Festivity festivity : festivities.values()) {
			DateTime myStartDate = DateUtils.parseDate(festivity.getStarDate());
			int result = DateTimeComparator.getDateOnlyInstance().compare(startDate, myStartDate);
			if (result == 0) {
				filteredList.add(festivity);
			}
		}
		return filteredList;
	}

	/**
	 * Return a <code>List</code> of <code>Festivity</code> with festivities in the database that occur
	 * between the specified data range (inclusive).
	 */
	public List<Festivity> getFestivitiesByDateRange(String startDateStr, String endDateStr) {
		ArrayList<Festivity> filteredList = new ArrayList<Festivity>();
		DateUtils.validDataRangeSimpleFormat(startDateStr, endDateStr);		
		DateTime startDate = DateUtils.parseSimpleDate(startDateStr);
		DateTime endDate = DateUtils.parseSimpleDate(endDateStr);
		for (Festivity festivity : festivities.values()) {
			DateTime myStartDate = DateUtils.parseDate(festivity.getStarDate());
			DateTime myEndDate = DateUtils.parseDate(festivity.getEndDate());
			int startResult = DateTimeComparator.getDateOnlyInstance().compare(myStartDate, startDate);
			int endResult = DateTimeComparator.getDateOnlyInstance().compare(myEndDate, endDate);
			if (startResult >= 0 && endResult <= 0) {
				filteredList.add(festivity);
			}			
		}
		return filteredList;
	}

	/**
	 * Return a <code>List</code> of <code>Festivity</code> with festivities in the database that match
	 * the specified place name.
	 */
	public List<Festivity> getFestivitiesByPlaceName(String placeName) {
		ArrayList<Festivity> filteredList = new ArrayList<Festivity>();		
		for (Festivity festivity : festivities.values()) {
			if (festivity.getPlace().getName().equals(placeName)) {
				filteredList.add(festivity);
			}
		}
		return filteredList;
	}

	/**
	 * Return the created <code>Festivity</code> with the id and related data.
	 */
	public Festivity addFestivity(Festivity festivity) {
		DataValidation.checkFestivityData(festivity);		
		festivity.setId(festivities.size() + 1);
		Long placeId = festivity.getPlace().getId();
		festivity.setName(festivity.getName().trim().replace(" ", "-"));		
		festivity.setPlace(places.get(placeId));		
		festivities.put(festivity.getId(), festivity);
		return festivity;		
	}	

	/**
	 * Update a festivity name. Return the updated <code>Festivity</code> with the related data.
	 */
	public Festivity updateFestivityName(Festivity festivity) {
		Long festId = festivity.getId();
		if (festivities.containsKey(festId)) {
			if(!festivity.getName().isEmpty())
			festivity.setName(festivity.getName().trim().replace(" ", "-"));
			festivities.get(festId).setName(festivity.getName());
			return festivities.get(festId);
		}		
		return null;
	}

	/**
	 * Update a festivity start date. Return the updated <code>Festivity</code> with the related data. 
	 * Dates in festivity must be in "ddMMyyyy" format. 
	 */
	public Festivity updateFestivityStartDate(Festivity festivity) {
		Long festId = festivity.getId();
		if (festivities.containsKey(festId)) {
			DateTime myEndDate = DateUtils.parseDate(festivities.get(festId).getEndDate());
			DateTime newStartDate = DateUtils.parseSimpleDate(festivity.getStarDate());
			int result = DateTimeComparator.getDateOnlyInstance().compare(myEndDate, newStartDate);
			if (result > 0) {
				festivities.get(festId).setStarDate(newStartDate.toString());
				return festivities.get(festId);
			} else {
				Throw.badRequest(new ErrorMessage("Bad date range", 400, "doc.festivities.api.errors"));
			}
		}		
		return null;
	}

	/**
	 * Update a festivity end date and return the updated <code>Festivity</code> with the related data. 
	 * Dates in festivity must be in "ddMMyyyy" format.
	 */
	public Festivity updateFestivityEndDate(Festivity festivity) {
		Long festId = festivity.getId();
		if (festivities.containsKey(festId)) {
			DateTime myStartDate = DateUtils.parseDate(festivities.get(festId).getStarDate());
			DateTime newEndDate = DateUtils.parseSimpleDate(festivity.getEndDate());
			int result = DateTimeComparator.getDateOnlyInstance().compare(newEndDate, myStartDate);
			if (result > 0) {
				festivities.get(festId).setEndDate(newEndDate.toString());
				return festivities.get(festId);
			} else {				
				Throw.badRequest(new ErrorMessage("Bad date range", 400, "doc.festivities.api.errors"));
			}
			
		}		
		return null;
	}

	/**
	 * Update a festivity start/end date and return the updated <code>Festivity</code> with the related 
	 * Dates in festivity must be in "ddMMyyyy" format.
	 */
	public Festivity updateFestivityDateRange(Festivity festivity) {
		Long festId = festivity.getId();
		if (festivities.containsKey(festId)) {
			DateTime newStartDate = DateUtils.parseSimpleDate(festivity.getStarDate());
			DateTime newEndDate = DateUtils.parseSimpleDate(festivity.getEndDate());
			int result = DateTimeComparator.getDateOnlyInstance().compare(newStartDate, newEndDate);
			if (result < 0) {
				festivities.get(festId).setStarDate(newStartDate.toString());
				festivities.get(festId).setEndDate(newEndDate.toString());
				return festivities.get(festId);
			} else {
				Throw.badRequest(new ErrorMessage("Bad date range", 400, "doc.festivities.api.errors"));
			}
		}		
		return null;
	}	

	/**
	 * Update a festivity data and return the updated <code>Festivity</code> with the new 
	 * Date. Note that dates in festivity must be in "ddMMyyyy" format. Data can be the
	 * festivity name, start date or end date (or combinations of them)
	 */
	@Override
	public Festivity updateFestivity(Festivity festivity) {
		boolean updateDataRange = false;

		if (festivityIdExists(festivity.getId())) {
			
			if (festivity.getName() != null) {
				if(festivity.getName().isEmpty()){
					Throw.badRequest(new ErrorMessage("Empty name", 400, "doc.festivities.api.errors"));
				}
				updateFestivityName(festivity);
			}

			if (festivity.getStarDate() != null && festivity.getEndDate() != null) {				
				updateFestivityDateRange(festivity);
				updateDataRange = true;
			}

			// if there is not a date range, then change the specific date
			if (!updateDataRange) {
				if (festivity.getStarDate() != null) {					
					updateFestivityStartDate(festivity);
				}

				if (festivity.getEndDate() != null) {					
					updateFestivityEndDate(festivity);
				}
			}

			return festivities.get(festivity.getId());
		}
		
		return null;
	}

	private boolean festivityIdExists(Long id) {
		if(festivities.containsKey(id)){			
			return true; 
		} 
		
		Throw.notFound(new ErrorMessage("Festivity with id " + id +" not found", 404, "doc.festivities.api.errors"));
		return false;
	}		

}
