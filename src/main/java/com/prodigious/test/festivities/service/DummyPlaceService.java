package com.prodigious.test.festivities.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.prodigious.test.festivities.database.DummyDb;
import com.prodigious.test.festivities.model.Place;

/**
 * Offer add/get services for places into the database.
 * 
 * @author nmarin
 *
 */
public class DummyPlaceService implements IPlaceService {

	Map<Long, Place> places = DummyDb.getPlaces();	
	
	public List<Place> getAllPlaces() {
		return new ArrayList<Place>(places.values());
	}

	public Place addPlace(Place place) {		
		place.setId(places.size()+1);
		places.put(place.getId(), place);
		return place;
	}

}
