package com.prodigious.test.festivities.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.prodigious.test.festivities.model.ErrorMessage;
import com.prodigious.test.festivities.model.Festivity;
import com.prodigious.test.festivities.resources.beans.FestivitiesQueryBean;
import com.prodigious.test.festivities.service.DummyFestivityService;
import com.prodigious.test.festivities.service.IFestivityService;
import com.prodigious.test.festivities.utils.Throw;

/**
 * Manage every request for festivities resources and sub-resources. Responses 
 * can be sent in JSON/XML according to client request. * 
 * 
 * @author nmarin
 *
 */
@Path("/festivities")
@Consumes(value={MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
@Produces(value={MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
public class FestivityResource {

	IFestivityService festivityService = new DummyFestivityService();

	/**
	 * Return all festivities available in the database.
	 * 
	 * @param queryBean
	 * 		  FestivitiesQueryBean injected by Jersey when clients send query parameters in the url.
	 *        Query parameters can be <code>place<code>, <code>start<code>, <code>end<code> to query
	 *        festivities by place name, start date or end date, respectively. This method only
	 *        accepts dates in format "ddMMyyyy".
	 * 
	 * @return Response with a JSON/XML with all festivities available or a message response with HTTP status code 404 
	 * 		   when there is no data available.
	 * 
	 */
	@GET
	public Response getFestivities(@BeanParam FestivitiesQueryBean queryBean) {

		List<Festivity> festivitiesList;
		
		if (queryBean.getPlace() != null) {
			festivitiesList = festivityService.getFestivitiesByPlaceName(queryBean.getPlace());		
		} else if (queryBean.getStartDate() != null && queryBean.getEndDate() != null) {			
			festivitiesList = festivityService.getFestivitiesByDateRange(queryBean.getStartDate(), queryBean.getEndDate());			
		} else if (queryBean.getStartDate() != null) {
			festivitiesList = festivityService.getFestivitiesByStartDate(queryBean.getStartDate());
		} else {
			festivitiesList = festivityService.getAllFestivities();
		}

		GenericEntity<List<Festivity>> entity = new GenericEntity<List<Festivity>>(festivitiesList) {};
		if (!festivitiesList.isEmpty()) {
			return Response.ok(entity).build();
		}

		return notFound(new ErrorMessage("Data not found", 404, "doc.festivities.api.errors"), entity);
	}

	/**
	 * Return festivities which have the specified name. Note that several festivities can have the same
	 * name.
	 * 
	 * @param festivityName
	 *        String with the festivity name.
	 *        
	 * @return Response with a JSON/XML with all festivities which match the specified name or a message 
	 *         response with HTTP status code 404 when there is no data available.
	 */
	@GET
	@Path("/{festivityName}")
	public Response getFestivityByName(@PathParam("festivityName") String festivityName) {
		List<Festivity> festivitiesList = festivityService.getFestivityByName(festivityName);
		GenericEntity<List<Festivity>> entity = new GenericEntity<List<Festivity>>(festivitiesList) {};
		if (!festivitiesList.isEmpty()) {
			return Response.ok(entity).build();
		}
		
		String message = "";
		
		if(festivityName.equals("id")){			
			// if user searches festivity by id but forgets to put it in the url
			message = "Data not found. If searching a festivity by id, please use: {BASE_URL}/festivities/id/{festivityId}"; 
		} else {
			message = "Data not found. No festivities with name: " + festivityName;
		}
		
		return notFound(new ErrorMessage(message, 404, "doc.festivities.api.errors"), entity);
	}
	
	/**
	 * Return festivity by id. It receives the id as a String but then parses it to Long.
	 * 
	 * @param festivityId String The festivity id.
	 * @return Festivity The festivity
	 */
	@GET
	@Path("/id/{festivityId}")
	public Response getFestivityById(@PathParam("festivityId") String festivityId) {		
		try {
			Long id = Long.parseLong(festivityId);
			Festivity festivity = festivityService.getFestivityById(id);
			return Response
					.status(Status.OK)
					.entity(festivity)
					.build();
		} catch (NumberFormatException e) {
			Throw.badRequest(new ErrorMessage("Expected a numeric id but received: " + festivityId, 400, "doc.festivities.api.errors"));
			return null;
		}
		
	}
	

	/**
	 * Add a new festivity to the database. Note that but festivity data is required as XML/JSON in the body
	 * message. This method only accepts festivity dates in format "yyyy-MM-dd'T'HH:mm:ss.SSSZ".
	 * 
	 * @param festivity
	 * 		  Festivity to be stored in database. Note that clients can send the festivity data in JSON/XML
	 * 
	 * @return Response with festivity data received plus the assigned id and a HTTP status code 201 to confirm
	 *         creation.
	 * 		   
	 */
	@POST
	public Response addFestivity(Festivity festivity) {
		Festivity festivityReply = festivityService.addFestivity(festivity);
		return Response
				.status(Status.CREATED)
				.entity(festivityReply)
				.build();
	}

	/**
	 * Update a existent festivity into the database. The festivity id is required to perform the update.
	 * Note that the id is required as part of the URL but festivity data is required as XML/JSON in the body
	 * message. This method only accepts dates in format "ddMMyyyy"
	 * 
	 * @param id
	 *        long with the festivity id.
	 *        
	 * @param festivity
	 * 		  Festivity data. Clients can update just the festivity name, the start date or the end date 
	 *        (or combination of this fields) in the same message.
	 * 
	 * @return Response with the festivity data updated or different error messages depending on data integrity.
	 */
	@PUT
	@Path("/{festivityId}")
	public Response updateFestivity(@PathParam("festivityId") long id, Festivity festivity) {	
		festivity.setId(id);
		Festivity updatedFestivity = festivityService.updateFestivity(festivity);
		return Response
				.status(Status.OK)
				.entity(updatedFestivity)
				.build();
	}

	/**
	 * Creates and send a HTTP 404 Response (not found) with an specified error message an entity.
	 * @param errorMessage
	 *        String with customized error message.
	 *        
	 * @param entity
	 *        Data to be sent to clients.
	 *        
	 * @return The Response.
	 */
	private Response notFound(ErrorMessage errorMessage, Object entity) {
		return Response
				.status(Status.NOT_FOUND)
				.entity(entity)
				.entity(errorMessage).build();
	}

}
