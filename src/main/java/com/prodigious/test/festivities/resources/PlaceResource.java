package com.prodigious.test.festivities.resources;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.prodigious.test.festivities.model.Place;
import com.prodigious.test.festivities.service.DummyPlaceService;
import com.prodigious.test.festivities.service.IPlaceService;

@Path("/places")
@Produces(value={MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
public class PlaceResource {

	IPlaceService placeService = new DummyPlaceService();
	
	@GET
	public List<Place> getPlaces(){
		return placeService.getAllPlaces();
	}	
	
}
