package com.prodigious.test.festivities.resources.beans;

import javax.ws.rs.QueryParam;

public class FestivitiesQueryBean {

	@QueryParam("place") String place;
	@QueryParam("start") String startDate;
	@QueryParam("end") String endDate;	

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

}
