package com.prodigious.test.festivities.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data model for places. A JSON/XML representations of this class 
 * is sent to clients when they query the API for places.
 * 
 * @author nmarin
 *
 */
@XmlRootElement
public class Place {

	private long id;	
	private String name;

	public Place() {
	}

	public Place(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "ID: " + id + " NAME: " + name;
	}

}
