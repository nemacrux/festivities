package com.prodigious.test.festivities.model;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Data model for festivities. A JSON/XML representations of this class 
 * is sent to clients when they query the API for festivities.
 * 
 * @author nmarin
 *
 */
@XmlRootElement
public class Festivity {

	private long id;
	private String name;
	private Place place;
	private String starDate;
	private String endDate;	

	public Festivity() {
	}

	/**
	 * Constructor.
	 * 
	 * @param place
	 *        Place An instance representing festivity place.
	 */
	public Festivity(long id, String name, String starDate, String endDate, Place place) {
		this.id = id;
		this.name = name;
		this.starDate = starDate;
		this.endDate = endDate;
		this.place = place;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Place getPlace() {
		return place;
	}

	public void setPlace(Place place) {
		this.place = place;
	}

	public String getStarDate() {
		return starDate;
	}

	public void setStarDate(String starDate) {
		this.starDate = starDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}	

	@Override
	public String toString() {
		return "ID: " + id + " NAME: " + name + " PLACE: " + ( place != null?place.getName():"null") + " START: " + starDate + " END: "
				+ endDate;
	}

}
