package com.prodigious.test.festivities.database;

import java.util.HashMap;
import java.util.Map;

import com.prodigious.test.festivities.model.Festivity;
import com.prodigious.test.festivities.model.Place;

/**
 * Dummy database based on maps to hold entities in-memory.
 * @author nmarin
 *
 */
public class DummyDb {

	private static Map<Long, Festivity> festivities = new HashMap<Long, Festivity>();
	private static Map<Long, Place> places = new HashMap<Long, Place>();
	
	public static Map<Long, Festivity> getFestivities() {
		return festivities;
	}

	public static Map<Long, Place> getPlaces() {
		return places;
	}

}
