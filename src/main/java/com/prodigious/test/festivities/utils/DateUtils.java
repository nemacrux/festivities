package com.prodigious.test.festivities.utils;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.prodigious.test.festivities.model.ErrorMessage;

/**
 * Offer several methods to parse or validate DateTime objects.
 * 
 * @author nmarin
 *
 */
public class DateUtils {

	// formaters to deal with dates
	public static final DateTimeFormatter dateFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ").withZoneUTC();
	public static final DateTimeFormatter simpleDateformatter = DateTimeFormat.forPattern("ddMMyyyy").withZoneUTC();

	/**
	 * Parse a text that represents a date with format yyyy-MM-dd'T'HH:mm:ss.SSSZ to a 
	 * new DateTime object. The new format of the date will be "yyyy-MM-dd'T'00:00:00.SSSZ". 
	 * 
	 * @param text Text to parse.
	 * @return DateTime The parsed day time.
	 */
	public static DateTime parseDate(String text) {
		try {
			DateTime date = dateFormatter.parseDateTime(text);
			return date;
		} catch (Exception ex) {
			Throw.badRequest(new ErrorMessage("Bad date format", 400, "doc.festivities.api.errors"));
			return null;
		}
	}

	/**
	 * Parse a text that represents a date with format ddMMyyyy to a  new DateTime object. The new	 
	 * format of the date will be "yyyy-MM-dd'T'00:00:00.SSSZ".
	 * 
	 * @param String text to parse.
	 * @return DateTime the parsed DateTime object.
	 */
	public static DateTime parseSimpleDate(String text) {
		try {
			DateTime date = simpleDateformatter.parseDateTime(text);
			return date;
		} catch (Exception ex) {
			Throw.badRequest(new ErrorMessage("Bad date format", 400, "doc.festivities.api.errors"));
			return null;
		}
	}

	/**
	 * Compare two strings that represent a date with format <b>yyyy-MM-dd'T'HH:mm:ss.SSSZ</b>
	 * and return <code>true</code> if the end date is greater than the star date. <b>It only 
	 * compares the yyyy-MM-dd fields</b>.
	 * 
	 * @param starDateText String the initial date.
	 * @param endDateText String the final date
	 * @return Boolean <code>true</code> if final date > initial date. <code>false</code> in
	 * other case.
	 */
	public static boolean validDataRange(String starDateText, String endDateText) {
		DateTime startDate = parseDate(starDateText);
		DateTime endDate = parseDate(endDateText);
		return validateDate(startDate, endDate);		
	}
	
	/**
	 * Compare two strings that represent a date with format <b>ddMMyyyy</b> and return <code>true</code> 
	 * if the end date is greater than the star date.
	 * 
	 * @param starDateText String the initial date.
	 * @param endDateText String the final date
	 * @return boolean <code>true</code> if final date > initial date. <code>false</code> in
	 * other case.
	 */
	public static boolean validDataRangeSimpleFormat(String starDateText, String endDateText) {
		DateTime startDate = parseSimpleDate(starDateText);
		DateTime endDate = parseSimpleDate(endDateText);
		return validateDate(startDate, endDate);
	}
	
	/**
	 * Compare two DateTime objects.
	 * 
	 * @param startDate DateTime the initial date.
	 * @param endDate DateTime the final date.
	 * @return boolean <code>true</code> if endDate > startDate
	 */
	private static boolean validateDate(DateTime startDate, DateTime endDate){
		int result = DateTimeComparator.getDateOnlyInstance().compare(endDate, startDate);
		if (result > 0) {
			return true;
		} else {
			Throw.badRequest(new ErrorMessage("Bad date range", 400, "doc.festivities.api.errors"));
			return false;
		}
	}

}
