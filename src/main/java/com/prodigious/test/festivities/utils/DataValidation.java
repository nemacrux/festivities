package com.prodigious.test.festivities.utils;

import java.util.Map;

import javax.ws.rs.BadRequestException;

import com.prodigious.test.festivities.database.DummyDb;
import com.prodigious.test.festivities.model.ErrorMessage;
import com.prodigious.test.festivities.model.Festivity;
import com.prodigious.test.festivities.model.Place;

/**
 * Offer methods to validate data and throw BadRequestException in case it finds
 * bad or missing data.
 * 
 * @author nmarin
 *
 */
public class DataValidation {

	private static Map<Long, Place> places = DummyDb.getPlaces();

	/**
	 * Check if the given <code>Festivity</code> has the correct data to be
	 * created or updated. It checks if the place exits, checks the name and if
	 * data range is valid.
	 * 
	 * @param festivity
	 *            the <code>Festivity</code>
	 * @throws BadRequestException
	 *             if finds missing or bad data.
	 */
	public static void checkFestivityData(Festivity festivity) {

		if (festivity.getPlace() == null) {
			Throw.badRequest(new ErrorMessage("Bad or missing place", 400, "doc.festivities.api.errors"));
		} else {
			checkPlaceData(festivity);
		}

		if (festivity.getName() == null || festivity.getName().isEmpty()) {
			Throw.badRequest(new ErrorMessage("Empty or missing name", 400, "doc.festivities.api.errors"));
		}

		if (festivity.getStarDate() == null || festivity.getStarDate().isEmpty()) {
			Throw.badRequest(new ErrorMessage("Empty or missing start date", 400, "doc.festivities.api.errors"));
		}

		if (festivity.getEndDate() == null || festivity.getEndDate().isEmpty()) {
			Throw.badRequest(new ErrorMessage("Empy or missing end date", 400, "doc.festivities.api.errors"));
		}

		DateUtils.validDataRange(festivity.getStarDate(), festivity.getEndDate());		
	}

	/**
	 * Checks if the place name given in the <code>Festivity<code> belongs to a
	 * correct place.
	 * 
	 * @param festivity
	 */
	public static void checkPlaceData(Festivity festivity) {

		if (places.isEmpty()) {
			Throw.notFound(new ErrorMessage("There is not registered places", 404, "doc.festivities.api.errors"));
		}

		Long placeId = festivity.getPlace().getId();

		if (placeId == null || placeId <= 0) {
			Throw.badRequest(new ErrorMessage("Bad or missing place id", 400, "doc.festivities.api.errors"));
		}

		if (!places.containsKey(placeId)) {
			Throw.notFound(new ErrorMessage("Place id not found", 404, "doc.festivities.api.errors"));
		}

	}

}
