package com.prodigious.test.festivities.utils;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.prodigious.test.festivities.model.ErrorMessage;

/**
 * Offer helper methods to throw quickly specific exceptions.
 * 
 * @author nmarin
 *
 */
public class Throw {	

	/**
	 * Throw a <code>NotFoundException</code> with the given message. It also adds a 
	 * <code>ErrorMessage</code> object with the given message, a custom error code and a
	 * documentaion URL of the error.
	 * 
	 * @param message
	 */
	public static void notFound(ErrorMessage errorMessage) {
		throw new NotFoundException(errorMessage.getErrorMessage(), Response
				.status(Status.NOT_FOUND)
				.entity(errorMessage)
				.build());
	}
	
	/**
	 * Throw a <code>BadRequestException</code> with the given message. It also adds a 
	 * <code>ErrorMessage</code> object with the given message, a custom error code and a
	 * documentaion URL of the error.
	 * 
	 * @param message
	 */
	public static void badRequest(ErrorMessage errorMessage) {
		throw new BadRequestException(errorMessage.getErrorMessage(), Response
				.status(Status.BAD_REQUEST)
				.entity(errorMessage)
				.build());		
	}		

}
