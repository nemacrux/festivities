package com.prodigious.test.festivities.service.test;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.prodigious.test.festivities.database.DummyDb;
import com.prodigious.test.festivities.model.Festivity;
import com.prodigious.test.festivities.model.Place;
import com.prodigious.test.festivities.service.DummyFestivityService;
import com.prodigious.test.festivities.service.DummyPlaceService;
import com.prodigious.test.festivities.service.IFestivityService;
import com.prodigious.test.festivities.service.IPlaceService;

public class DummyFestivityServiceBasicTest {

	private static final String NO_EXISTENT_NAME_EVENT = "no-existent-name-event";
	private static final String END_DATE = "2016-06-02T00:00:00.001Z";
	private static final String SIMPLE_END_DATE = "02062016";
	private static final String START_DATE = "2016-06-01T00:00:00.001Z";
	private static final String SIMPLE_START_DATE = "01062016";
	private static IFestivityService festivityService = new DummyFestivityService();;
	private static IPlaceService placeService = new DummyPlaceService();
	private static final String PLACE_NAME = "test-place-1";
	private static final long PLACE_ID = 1;
	private static final String EVENT_NAME = "test-event-1";
	private static final DateTimeFormatter simpleDateformatter = DateTimeFormat.forPattern("ddMMyyyy").withZoneUTC();

	//TODO design test cases when method should throw an exception
	
	@Before
	public void cleanFestivitiesDatabase() {
		DummyDb.getFestivities().clear();
	}

	@BeforeClass
	public static void loadData() {
		Place place = new Place(PLACE_ID, PLACE_NAME);
		placeService.addPlace(place);
	}

	@Test
	public void TestAddFestivity() {
		Festivity festivity = createFestivity();
		Festivity newFestivity = festivityService.addFestivity(festivity);
		Assert.assertNotNull(newFestivity);
		Assert.assertTrue(newFestivity.getId() > 0);
	}

	@Test
	public void testGetAllFestivities() {
		addTestFestivity();
		List<Festivity> festivitiesList = festivityService.getAllFestivities();
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() > 0);
	}

	@Test
	public void testGetFestivityByName() {
		addTestFestivity();
		List<Festivity> festivitiesList = festivityService.getFestivityByName(EVENT_NAME);
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() > 0);
		festivitiesList = festivityService.getFestivityByName(NO_EXISTENT_NAME_EVENT);
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() == 0);
	}

	@Test
	public void testGetFestivityByInexistentName() {
		addTestFestivity();
		List<Festivity> festivitiesList = festivityService.getFestivityByName(NO_EXISTENT_NAME_EVENT);
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() == 0);
	}

	@Test
	public void testGetFestivitiesByStartDate() {
		addTestFestivity();
		List<Festivity> festivitiesList = festivityService.getFestivitiesByStartDate(SIMPLE_START_DATE);
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() == 1);
	}

	@Test
	public void testGetFestivitiesByDateRange() {
		addTestFestivity();
		List<Festivity> festivitiesList = festivityService.getFestivitiesByDateRange(SIMPLE_START_DATE,
				SIMPLE_END_DATE);
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() == 1);
	}

	@Test
	public void testGetFestivitiesByPlaceName() {
		addTestFestivity();
		List<Festivity> festivitiesList = festivityService.getFestivitiesByPlaceName(PLACE_NAME);
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() == 1);
	}

	@Test
	public void testUpdateFestivityName() {
		final String NEW_NAME = EVENT_NAME + "-new-name";
		Festivity dbFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(dbFestivity.getId());
		newDatafestivity.setName(NEW_NAME);		
		Festivity updatedFestivity = festivityService.updateFestivityName(newDatafestivity);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(NEW_NAME, updatedFestivity.getName());
	}
	
	@Test
	public void testUpdateFestivityStartDate(){
		final String NEW_START_DATE = "01012010";
		Festivity dbFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(dbFestivity.getId());
		newDatafestivity.setStarDate(NEW_START_DATE);		
		Festivity updatedFestivity = festivityService.updateFestivityStartDate(newDatafestivity);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(parseSimpleDate(NEW_START_DATE).toString(), updatedFestivity.getStarDate());
	}
	
	@Test
	public void testUpdateFestivityEndDate(){
		final String NEW_END_DATE = "01012020";
		Festivity dbFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(dbFestivity.getId());
		newDatafestivity.setEndDate(NEW_END_DATE);		
		Festivity updatedFestivity = festivityService.updateFestivityEndDate(newDatafestivity);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(parseSimpleDate(NEW_END_DATE).toString(), updatedFestivity.getEndDate());
	}
	
	@Test
	public void testUpdateFestivityDateRange(){
		final String NEW_START_DATE = "01012010";
		final String NEW_END_DATE = "01012020";
		Festivity dbFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(dbFestivity.getId());
		newDatafestivity.setStarDate(NEW_START_DATE);
		newDatafestivity.setEndDate(NEW_END_DATE);		
		Festivity updatedFestivity = festivityService.updateFestivityDateRange(newDatafestivity);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(parseSimpleDate(NEW_START_DATE).toString(), updatedFestivity.getStarDate());
		Assert.assertEquals(parseSimpleDate(NEW_END_DATE).toString(), updatedFestivity.getEndDate());
	}
	
	@Test
	public void testUpdateFestivity(){
		final String NEW_NAME = EVENT_NAME + "-new-name";
		final String NEW_START_DATE = "01012010";
		final String NEW_END_DATE = "01012020";
		Festivity dbFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(dbFestivity.getId());
		newDatafestivity.setName(NEW_NAME);		
		newDatafestivity.setStarDate(NEW_START_DATE);
		newDatafestivity.setEndDate(NEW_END_DATE);		
		Festivity updatedFestivity = festivityService.updateFestivity(newDatafestivity);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(NEW_NAME, updatedFestivity.getName());
		Assert.assertEquals(parseSimpleDate(NEW_START_DATE).toString(), updatedFestivity.getStarDate());
		Assert.assertEquals(parseSimpleDate(NEW_END_DATE).toString(), updatedFestivity.getEndDate());
	}
	

	public Festivity createFestivity() {
		Festivity festivity = new Festivity();
		festivity.setName(EVENT_NAME);
		Place place = new Place(PLACE_ID, "");
		festivity.setPlace(place);
		festivity.setStarDate(START_DATE);
		festivity.setEndDate(END_DATE);
		return festivity;
	}

	private Festivity addTestFestivity() {
		Festivity festivity = createFestivity();
		return festivityService.addFestivity(festivity);
	}
	
	private DateTime parseSimpleDate(String stringDate){		
		try {			
			DateTime date = simpleDateformatter.parseDateTime(stringDate);
			return date;
		} catch (Exception ex){
			throw new IllegalArgumentException("Bad date format: "+stringDate);			
		}		
	}
	

}
