package com.prodigious.test.festivities.resources.test;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.DeploymentContext;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.external.ExternalTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainer;
import org.glassfish.jersey.test.spi.TestContainerFactory;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;
import org.junit.Test;

import com.prodigious.test.festivities.model.Festivity;
import com.prodigious.test.festivities.model.Place;
import com.prodigious.test.festivities.resources.FestivityResource;

public class FestivitiesResourcesTest extends JerseyTest {

	private static final String NO_EXISTENT_NAME_EVENT = "no-existent-name-event";
	private static final String END_DATE = "2016-06-02T00:00:00.001Z";
	private static final String START_DATE = "2016-06-01T00:00:00.001Z";
	private static final String SIMPLE_START_DATE = "01062016";
	private static final String SIMPLE_END_DATE = "02062016";
	private static final long PLACE_ID = 1;
	private static final String PLACE_NAME = "Clark's-castle";
	private static final String EVENT_NAME = "test-event-1";
	private static final DateTimeFormatter simpleDateformatter = DateTimeFormat.forPattern("ddMMyyyy").withZoneUTC();

	@Override
	public TestContainerFactory getTestContainerFactory() {
		return new ExternalTestContainerFactory() {

			@Override
			public TestContainer create(URI baseUri, DeploymentContext context) throws IllegalArgumentException {
				try {
					baseUri = new URI("http://localhost:8080/festivities/webapi");
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
				return super.create(baseUri, context);
			}
		};
	}

	@Override
	protected Application configure() {
		return new ResourceConfig(FestivityResource.class);
	}

	@Test
	public void testGetFestivities() {
		addTestFestivity();
		Response response = target("/festivities").request().get(Response.class);
		List<Festivity> festList = response.readEntity(new GenericType<List<Festivity>>() {});
		Assert.assertNotNull(festList);		
		Assert.assertTrue(festList.size() > 0);		
	}	

	 @Test
	 public void testGetFestivityByName() {
	 	addTestFestivity();
	 	Response response = target("/festivities/"+EVENT_NAME).request().get(Response.class);
	 	List<Festivity> festivitiesList = response.readEntity(new GenericType<List<Festivity>>() {});
		Assert.assertNotNull(festivitiesList);
		Assert.assertTrue(festivitiesList.size() > 0);
		Assert.assertEquals(Status.OK.getStatusCode(), response.getStatus());	
	 }
	 
	 @Test
	 public void testGetFestivityByInexistentName() {
		 Response response = target("/festivities/"+NO_EXISTENT_NAME_EVENT).request().get(Response.class);
		 Assert.assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());
	 }
	 
	 @Test
		public void testGetFestivitiesByStartDate() {
			addTestFestivity();
			Response response = target("/festivities").queryParam("start", SIMPLE_START_DATE).request().get(Response.class);
			List<Festivity> festivitiesList = response.readEntity(new GenericType<List<Festivity>>() {});
			Assert.assertNotNull(festivitiesList);
			Assert.assertTrue(festivitiesList.size() >= 1);
		}

	 @Test
	 public void testGetFestivitiesByDateRange() {
		 addTestFestivity();
		 Response response = target("/festivities").queryParam("start", SIMPLE_START_DATE).queryParam("end",SIMPLE_END_DATE).request().get(Response.class);
		 List<Festivity> festivitiesList = response.readEntity(new GenericType<List<Festivity>>() {});
		 Assert.assertNotNull(festivitiesList);
		 Assert.assertTrue(festivitiesList.size() >= 1);
	 }
	 
	 @Test
		public void testGetFestivitiesByPlaceName() {
		 //TODO this test is using PLACE_NAME which value depends on those in XML file values
		 Response response = target("/festivities").queryParam("place", PLACE_NAME).request().get(Response.class);
			addTestFestivity();
			List<Festivity> festivitiesList = response.readEntity(new GenericType<List<Festivity>>() {});
			Assert.assertNotNull(festivitiesList);
			Assert.assertTrue(festivitiesList.size() >= 1);
		}	 
	 
	@Test
	public void testAddFestivity() {
		Response response = target("/festivities").request()
				.post(Entity.entity(createFestivity(), MediaType.APPLICATION_JSON));
		Assert.assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
		Festivity newFestivity = response.readEntity(Festivity.class);
		Assert.assertNotNull(newFestivity);
		Assert.assertTrue(newFestivity.getId() > 0);
	}
	
	@Test
	public void testUpdateFestivityName() {
		final String NEW_NAME = EVENT_NAME + "-new-name";
		Festivity toSendFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(toSendFestivity.getId());
		newDatafestivity.setName(NEW_NAME);		
		Response response = target("/festivities/"+toSendFestivity.getId()).request().put(Entity.entity(newDatafestivity, MediaType.APPLICATION_JSON));
		Festivity updatedFestivity = response.readEntity(Festivity.class);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(NEW_NAME, updatedFestivity.getName());
	}
	
	@Test
	public void testUpdateFestivityStartDate(){
		final String NEW_START_DATE = "01012010";
		Festivity toSendFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(toSendFestivity.getId());
		newDatafestivity.setStarDate(NEW_START_DATE);		
		Response response = target("/festivities/"+toSendFestivity.getId()).request().put(Entity.entity(newDatafestivity, MediaType.APPLICATION_JSON));
		Festivity updatedFestivity = response.readEntity(Festivity.class);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(parseSimpleDate(NEW_START_DATE).toString(), updatedFestivity.getStarDate());
	}
	
	@Test
	public void testUpdateFestivityEndDate(){
		final String NEW_END_DATE = "01012020";
		Festivity toSendFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(toSendFestivity.getId());
		newDatafestivity.setEndDate(NEW_END_DATE);		
		Response response = target("/festivities/"+toSendFestivity.getId()).request().put(Entity.entity(newDatafestivity, MediaType.APPLICATION_JSON));
		Festivity updatedFestivity = response.readEntity(Festivity.class);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(parseSimpleDate(NEW_END_DATE).toString(), updatedFestivity.getEndDate());
	}
	
	@Test
	public void testUpdateFestivityDateRange(){
		final String NEW_START_DATE = "01012010";
		final String NEW_END_DATE = "01012020";
		Festivity toSendFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(toSendFestivity.getId());
		newDatafestivity.setStarDate(NEW_START_DATE);
		newDatafestivity.setEndDate(NEW_END_DATE);		
		Response response = target("/festivities/"+toSendFestivity.getId()).request().put(Entity.entity(newDatafestivity, MediaType.APPLICATION_JSON));
		Festivity updatedFestivity = response.readEntity(Festivity.class);
		Assert.assertNotNull(updatedFestivity);
		Assert.assertEquals(parseSimpleDate(NEW_START_DATE).toString(), updatedFestivity.getStarDate());
		Assert.assertEquals(parseSimpleDate(NEW_END_DATE).toString(), updatedFestivity.getEndDate());
	}
	
	@Test
	public void testUpdateFestivity(){
		final String NEW_NAME = EVENT_NAME + "-new-name";
		final String NEW_START_DATE = "01012010";
		final String NEW_END_DATE = "01012020";
		Festivity toSendFestivity = addTestFestivity();
		Festivity newDatafestivity = new Festivity();
		newDatafestivity.setId(toSendFestivity.getId());
		newDatafestivity.setName(NEW_NAME);		
		newDatafestivity.setStarDate(NEW_START_DATE);
		newDatafestivity.setEndDate(NEW_END_DATE);		
		Response response = target("/festivities/"+toSendFestivity.getId()).request().put(Entity.entity(newDatafestivity, MediaType.APPLICATION_JSON));
		Festivity updatedFestivity = response.readEntity(Festivity.class);
		Assert.assertEquals(NEW_NAME, updatedFestivity.getName());
		Assert.assertEquals(parseSimpleDate(NEW_START_DATE).toString(), updatedFestivity.getStarDate());
		Assert.assertEquals(parseSimpleDate(NEW_END_DATE).toString(), updatedFestivity.getEndDate());
	}
	
	
	public Festivity createFestivity() {
		Festivity festivity = new Festivity();
		festivity.setName(EVENT_NAME);
		Place place = new Place(PLACE_ID, "");
		festivity.setPlace(place);
		festivity.setStarDate(START_DATE);
		festivity.setEndDate(END_DATE);
		return festivity;
	}

	private Festivity addTestFestivity() {
		Response response = target("/festivities")
							.request()
							.post(Entity.entity(createFestivity(), MediaType.APPLICATION_JSON));
		
		return response.readEntity(Festivity.class);
	}
	
	private DateTime parseSimpleDate(String stringDate){		
		try {			
			DateTime date = simpleDateformatter.parseDateTime(stringDate);
			return date;
		} catch (Exception ex){
			throw new IllegalArgumentException("Bad date format: "+stringDate);			
		}		
	}

}
